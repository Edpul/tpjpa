package com.training.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.training.model.Adresse;
import com.training.model.Employee;

public interface AdresseRepository {
	
	List<Adresse> findAllByPrice(double note, Pageable pageable);

}
