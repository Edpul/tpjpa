package com.training.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Entity
@Scope("prototype")
@Table(name="evaluation")
public class Evaluation {
	
	@Id 
	@GeneratedValue (strategy=GenerationType.AUTO)
	private int id;
	private int annee;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	private int moyenne;

	


	@ManyToOne
	@JoinColumn (name="employee_id")
	private Employee employeeEva;
	
	public Employee getEmployeeEva() {
		return employeeEva;
	}

	public void setEmployeeEva(Employee employeeEva) {
		this.employeeEva = employeeEva;
	}

	public Evaluation() {
		
	}
	


	
	public Evaluation(int annee, int sport, int social, int performance, int assiduite, int moyenne) {
		this.annee = annee;
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.moyenne = moyenne;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	public int getSport() {
		return sport;
	}
	public void setSport(int sport) {
		this.sport = sport;
	}
	public int getSocial() {
		return social;
	}
	public void setSocial(int social) {
		this.social = social;
	}
	public int getPerformance() {
		return performance;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
		
	public int getAssiduite() {
		return assiduite;
	}

	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}
	
	public int getMoyenne() {	
		moyenne = ((int)(getSport() + getAssiduite()+ getPerformance()+ getSocial()))/4; 
		return moyenne;
	}
	

	

	
	
	

}
